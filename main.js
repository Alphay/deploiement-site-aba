(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/ad-page/ad-page.component.html":
/*!************************************************!*\
  !*** ./src/app/ad-page/ad-page.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container shadow-lg\">  \n  <form id=\"contact\" action=\"\" method=\"post\">\n    <h3>Déposer une annonce</h3>\n    <h4>Publiez votre produit pour lui donner plus de visibilité!</h4>\n    <fieldset>\n      <label>Nom</label>\n      <input placeholder=\"Votre Nom\" type=\"text\" name=\"name\" id=\"name\" tabindex=\"2\" required (change)=\"checkName($event);\"mkn/>\n    </fieldset>\n    <fieldset>\n      <label>Adresse Email</label>\n      <input placeholder=\"Votre Adresse Email\" type=\"email\" name=\"email\" id=\"email\" tabindex=\"2\" required (change)=\"checkEmail($event);\"mkn/>\n    </fieldset>\n    <fieldset>\n      <label>Téléphone</label>\n      <input placeholder=\"Votre Telephone\" type=\"tel\" name=\"phoneNumber\" id=\"phoneNumber\" tabindex=\"3\" required (change)=\"checkPhoneNumber($event);\"mkn/>\n    </fieldset>\n    <fieldset>\n      <label>Titre Produit</label>\n      <input placeholder=\"Nom de votre produit\" type=\"text\" name=\"title\" id=\"title\" tabindex=\"3\" required (change)=\"checkTitle($event);\"mkn/>\n    </fieldset>\n    <fieldset>\n      <label>Catégorie</label>\n      <input placeholder=\"type de produit\" type=\"text\" name=\"category\" id=\"category\" tabindex=\"3\" required (change)=\" checkCategory($event);\"mkn/>\n    </fieldset>\n    <fieldset>\n      <label>Prix</label>\n      <input placeholder=\"Prix\" type=\"text\" name=\"price\" id=\"price\" tabindex=\"3\" required (change)=\"checkPrice($event);\"mkn/>\n    </fieldset>\n    <label>Description</label>\n    <fieldset>\n      <textarea placeholder=\"Description de votre produit....\" tabindex=\"5\" name=\"description\" id=\"description\" required (change)=\"checkDescription($event);\"mkn></textarea>\n    </fieldset>\n    <fieldset>\n      <button (click)=\"newProduct()\" name=\"submit\" type=\"submit\" id=\"contact-submit\" data-submit=\"...Sending\">Valider</button>\n    </fieldset>\n  </form> \n</div>\n<div class=\"row img-picture\">\n  <div class=\"picture\"  *ngFor='let url of urls'>\n    <img  [src]=\"url\" height=\"100\" alt=\"telecharger les photos\" srcset=\"\">\n    <input class=\"input-picture\"  type='file' (change)=\"onSelectFile($event)\" multiple  accept=\"image/bmp,image/gif,image/jpeg,image/png,image/svg,image/x-ms-bmp\" name=\"image2\" id=\"image2\">\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/ad-page/ad-page.component.scss":
/*!************************************************!*\
  !*** ./src/app/ad-page/ad-page.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600);\n* {\n  margin: 0;\n  padding: 0;\n  box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  -webkit-font-smoothing: antialiased;\n  -moz-font-smoothing: antialiased;\n  -o-font-smoothing: antialiased;\n  font-smoothing: antialiased;\n  text-rendering: optimizeLegibility; }\nbody {\n  font-family: \"Open Sans\", Helvetica, Arial, sans-serif;\n  font-weight: 300;\n  font-size: 12px;\n  line-height: 30px;\n  color: #777;\n  background: #4167b2; }\n.container {\n  max-width: 400px;\n  width: 100%;\n  margin: 0 auto;\n  position: relative; }\n#contact input[type=\"text\"], #contact input[type=\"email\"], #contact input[type=\"tel\"], #contact input[type=\"url\"], #contact textarea, #contact button[type=\"submit\"] {\n  font: 400 12px/16px \"Open Sans\", Helvetica, Arial, sans-serif; }\n#contact {\n  background: #F9F9F9;\n  padding: 25px;\n  margin-bottom: 30px; }\n#contact h3 {\n  color: #4167b2;\n  display: block;\n  font-size: 30px;\n  font-weight: 400;\n  margin-top: 40px; }\n#contact h4 {\n  margin: 5px 0 15px;\n  display: block;\n  font-size: 13px; }\nfieldset {\n  border: medium none !important;\n  margin: 0 0 10px;\n  min-width: 100%;\n  padding: 0;\n  width: 100%; }\n#contact input[type=\"text\"], #contact input[type=\"email\"], #contact input[type=\"tel\"], #contact input[type=\"url\"], #contact textarea {\n  width: 100%;\n  border: 1px solid #CCC;\n  background: #FFF;\n  margin: 0 0 5px;\n  padding: 10px; }\n#contact input[type=\"text\"]:hover, #contact input[type=\"email\"]:hover, #contact input[type=\"tel\"]:hover, #contact input[type=\"url\"]:hover, #contact textarea:hover {\n  transition: border-color 0.3s ease-in-out;\n  border: 1px solid #AAA; }\n#contact textarea {\n  height: 100px;\n  max-width: 100%;\n  resize: none; }\n#contact button[type=\"submit\"] {\n  cursor: pointer;\n  width: 100%;\n  border: none;\n  background: #4167b2;\n  color: #FFF;\n  margin: 0 0 5px;\n  padding: 10px;\n  font-size: 15px; }\n#contact button[type=\"submit\"]:hover {\n  background: #09C;\n  transition: background-color 0.3s ease-in-out; }\n#contact button[type=\"submit\"]:active {\n  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5); }\n#contact input:focus, #contact textarea:focus {\n  outline: 0;\n  border: 1px solid #999; }\n::-webkit-input-placeholder {\n  color: #888; }\n:-moz-placeholder {\n  color: #888; }\n::-moz-placeholder {\n  color: #888; }\n:-ms-input-placeholder {\n  color: #888; }\n.picture {\n  bottom: 86px;\n  position: relative;\n  left: 185px; }\n@media (max-width: 575.98px) {\n  .picture {\n    bottom: 86px;\n    position: relative;\n    left: -194px; } }\n.input-picture {\n  position: absolute;\n  top: 0;\n  left: 0;\n  display: block;\n  height: 100%;\n  width: 100%;\n  opacity: 0;\n  filter: alpha(opacity=0);\n  cursor: pointer; }\n.img-picture {\n  position: relative;\n  width: 80%;\n  margin: auto;\n  left: 314px;\n  margin-top: 110px; }\n.shadow-lg {\n  top: 114px;\n  box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.175) !important; }\n"

/***/ }),

/***/ "./src/app/ad-page/ad-page.component.ts":
/*!**********************************************!*\
  !*** ./src/app/ad-page/ad-page.component.ts ***!
  \**********************************************/
/*! exports provided: AdPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdPageComponent", function() { return AdPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/e-com-store-api.service */ "./src/app/services/e-com-store-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdPageComponent = /** @class */ (function () {
    function AdPageComponent(service, router) {
        this.service = service;
        this.router = router;
        this.urls = ['../../assets/icon-picture.svg'];
        this.produits = [];
    }
    AdPageComponent.prototype.ngOnInit = function () {
    };
    AdPageComponent.prototype.checkName = function (event) {
        console.log(event);
        this.name = event.target.value;
    };
    AdPageComponent.prototype.checkEmail = function (event) {
        console.log(event);
        this.email = event.target.value;
    };
    AdPageComponent.prototype.checkPhoneNumber = function (event) {
        console.log(event);
        this.phoneNumber = event.target.value.toString();
    };
    AdPageComponent.prototype.checkTitle = function (event) {
        console.log(event);
        this.title = event.target.value;
    };
    AdPageComponent.prototype.checkCategory = function (event) {
        console.log(event);
        this.category = event.target.value;
    };
    AdPageComponent.prototype.checkPrice = function (event) {
        console.log(event);
        this.price = parseInt(event.target.value);
    };
    AdPageComponent.prototype.checkDescription = function (event) {
        console.log(event);
        this.description = event.target.value;
    };
    /**Connexion d'un nouveau utilisateur */
    AdPageComponent.prototype.newProduct = function () {
        var product = { name: this.title, price: this.price, category: this.category, email: this.email, phoneNumber: this.phoneNumber, description: this.description, userName: this.name, images: this.newImage };
        console.log("TEST ", product);
        this.service.addProduct(product).subscribe(function (value) {
            console.log("value", value);
        });
    };
    AdPageComponent.prototype.onSelectFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var filesAmount = event.target.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.newImage = event.target["result"];
                    console.log(event.target["result"]);
                    _this.urls.push(event.target["result"]);
                };
                reader.readAsDataURL(event.target.files[i]);
            }
        }
    };
    AdPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ad-page',
            template: __webpack_require__(/*! ./ad-page.component.html */ "./src/app/ad-page/ad-page.component.html"),
            styles: [__webpack_require__(/*! ./ad-page.component.scss */ "./src/app/ad-page/ad-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_2__["EComStoreApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AdPageComponent);
    return AdPageComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _produit_list_produit_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./produit-list/produit-list.component */ "./src/app/produit-list/produit-list.component.ts");
/* harmony import */ var _produit_fiche_produit_fiche_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./produit-fiche/produit-fiche.component */ "./src/app/produit-fiche/produit-fiche.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var src_app_panier_panier_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/panier/panier.component */ "./src/app/panier/panier.component.ts");
/* harmony import */ var _user_account_user_account_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-account/user-account.component */ "./src/app/user-account/user-account.component.ts");
/* harmony import */ var _user_sign_up_user_sign_up_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user-sign-up/user-sign-up.component */ "./src/app/user-sign-up/user-sign-up.component.ts");
/* harmony import */ var _ad_page_ad_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ad-page/ad-page.component */ "./src/app/ad-page/ad-page.component.ts");
/* harmony import */ var _user_log_out_user_log_out_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user-log-out/user-log-out.component */ "./src/app/user-log-out/user-log-out.component.ts");
/* harmony import */ var _is_sign_in_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./is-sign-in.guard */ "./src/app/is-sign-in.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    { path: '', component: _produit_list_produit_list_component__WEBPACK_IMPORTED_MODULE_2__["ProduitListComponent"] },
    { path: 'user/:id', component: _produit_list_produit_list_component__WEBPACK_IMPORTED_MODULE_2__["ProduitListComponent"] },
    { path: 'produit-fiche/:id', component: _produit_fiche_produit_fiche_component__WEBPACK_IMPORTED_MODULE_3__["ProduitFicheComponent"] },
    { path: 'panier', component: src_app_panier_panier_component__WEBPACK_IMPORTED_MODULE_5__["PanierComponent"] },
    { path: 'user-account', component: _user_account_user_account_component__WEBPACK_IMPORTED_MODULE_6__["UserAccountComponent"] },
    { path: 'user-sign-up', component: _user_sign_up_user_sign_up_component__WEBPACK_IMPORTED_MODULE_7__["UserSignUpComponent"] },
    { path: 'user-log-out', component: _user_log_out_user_log_out_component__WEBPACK_IMPORTED_MODULE_9__["UserLogOutComponent"] },
    { path: 'ad-page', component: _ad_page_ad_page_component__WEBPACK_IMPORTED_MODULE_8__["AdPageComponent"], canActivate: [_is_sign_in_guard__WEBPACK_IMPORTED_MODULE_10__["IsSignInGuard"]] },
    // { path: 'movie', component:  MovieComponent},
    { path: '**', component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css\" integrity=\"sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB\" crossorigin=\"anonymous\">\n<body class=\"body\">\n  <app-header id=\"header\"></app-header>\n  <router-outlet></router-outlet>\n  <app-footer id=\"footer\"></app-footer>\n</body>\n\n\n\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  background-color: #eff0f2;\n  box-shadow: 0 8px 20px 0 rgba(0, 0, 0, 0.25); }\n\n#header {\n  position: fixed;\n  z-index: 999999; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _produit_list_produit_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./produit-list/produit-list.component */ "./src/app/produit-list/produit-list.component.ts");
/* harmony import */ var _produit_fiche_produit_fiche_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./produit-fiche/produit-fiche.component */ "./src/app/produit-fiche/produit-fiche.component.ts");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _panier_panier_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./panier/panier.component */ "./src/app/panier/panier.component.ts");
/* harmony import */ var src_app_services_produit_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/services/produit.service */ "./src/app/services/produit.service.ts");
/* harmony import */ var _user_account_user_account_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./user-account/user-account.component */ "./src/app/user-account/user-account.component.ts");
/* harmony import */ var _user_sign_up_user_sign_up_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./user-sign-up/user-sign-up.component */ "./src/app/user-sign-up/user-sign-up.component.ts");
/* harmony import */ var _ad_page_ad_page_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./ad-page/ad-page.component */ "./src/app/ad-page/ad-page.component.ts");
/* harmony import */ var _pipes_safe_pipe__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pipes/safe.pipe */ "./src/app/pipes/safe.pipe.ts");
/* harmony import */ var _user_log_out_user_log_out_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./user-log-out/user-log-out.component */ "./src/app/user-log-out/user-log-out.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















// import { SearchComponent } from './search/search.component';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _produit_list_produit_list_component__WEBPACK_IMPORTED_MODULE_4__["ProduitListComponent"],
                _produit_fiche_produit_fiche_component__WEBPACK_IMPORTED_MODULE_5__["ProduitFicheComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_9__["HeaderComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
                _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_12__["NotFoundComponent"],
                _panier_panier_component__WEBPACK_IMPORTED_MODULE_14__["PanierComponent"],
                _user_account_user_account_component__WEBPACK_IMPORTED_MODULE_16__["UserAccountComponent"],
                _user_sign_up_user_sign_up_component__WEBPACK_IMPORTED_MODULE_17__["UserSignUpComponent"],
                _ad_page_ad_page_component__WEBPACK_IMPORTED_MODULE_18__["AdPageComponent"],
                _pipes_safe_pipe__WEBPACK_IMPORTED_MODULE_19__["SafePipe"],
                _user_log_out_user_log_out_component__WEBPACK_IMPORTED_MODULE_20__["UserLogOutComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_6__["MatMenuModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_11__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]
            ],
            providers: [
                src_app_services_produit_service__WEBPACK_IMPORTED_MODULE_15__["ProduitService"]
                // PanierService
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!--Footer-->\n<footer class=\"footer mt-lg-4\">\n  <div class=\"container-full\">\n    <div class=\"container\">\n      <div class=\"d-flex justify-content-between\">\n        <div class=\"icon-social-r\">\n          <span class=\"icon mr-sm-3\">Retrouver-nous sur</span>\n          <a href=\"#\">\n            <img src=\"assets/icon-facebook.png\" alt=\"\" class=\"img-fluid\">\n          </a>\n          <a href=\"#\">\n            <img src=\"assets/icon-twitter.png\" alt=\"\" class=\"img-fluid\">\n          </a>\n          <a href=\"#\">\n            <img src=\"assets/images/icon-pinterest.png\" alt=\"\" class=\"img-fluid\">\n          </a>\n        </div>\n      </div>\n      <div  class=\"row border-footer mt-3\">\n        <ul class=\"col-6 col-md-2 col-lg-3\">\n          <li class=\"title-footer\"><a href=\"\">À PROPOS DE NOUS</a></li>\n          <li><a href=\"\">Qui sommes-nous ?</a></li>\n          <li><a href=\"\">Nous rejoindre</a></li>\n        </ul>\n        <ul class=\"col-6 col-md-2 col-lg-3\">\n          <li class=\"title-footer\"><a href=\"\">CONDITIONS GÉNÉRALES</a></li>\n          <li><a href=\"\">Conditions Générales de Vente & Règles de diffusion</a></li>\n          <li><a href=\"\">Conditions générales d’utilisation</a></li>\n          <li><a href=\"\"> Vie privée / données personnelles</a></li>\n        </ul>\n        <ul class=\"col-6 col-md-2 col-lg-3\">\n          <li class=\"title-footer\"><a href=\"\">NOS ACTUALITÉS</a></li>\n          <li><a href=\"\">Nos actions</a></li>\n          <li><a href=\"\">Newsletter</a></li>\n          <li><a href=\"\">Bons plans</a></li>\n          <li><a href=\"\">Annuaire des professionnels</a></li>\n        </ul>\n        <ul class=\"col-6 col-md-2 col-lg-3\">\n          <li class=\"title-footer\"><a href=\"\">PUBLICITÉS</a></li>\n          <li><a href=\"\">Professionnels & Particuliers</a></li>\n          <li><a href=\"\">Entrepreneurs</a></li>\n        </ul>\n        <ul class=\"col-6 col-md-2 col-lg-3\">\n          <li class=\"title-footer\"><a href=\"\">NOUS CONTACTER</a></li>\n          <li><a href=\"\">Mail: africaba@gmail.com</a></li>\n          <li><a href=\"\">Tél:+337 53063782</a></li>\n        </ul>\n        <img class=\"img-fluid map-africa\" src=\"../../assets/Guinea_in_Africa.svg\" alt=\"image de la carte de la Guinée\" srcset=\"\">\n      </div>\n      <div class=\"row d-flex justify-content-around bar-nav\">\n        <div >\n          <a href=\"#\">\n            <!--<img src=\"assets/images/icon-casq.png\" alt=\"\" class=\"img-fluid\">-->\n            <i class=\"fas fa-globe-africa fa-5x\"></i>\n          </a>\n        </div>\n        <div>\n          <a href=\"\">\n            <!--<img src=\"assets/images/icon-basket.png\" alt=\"\" class=\"img-fluid\">-->\n            <i class=\"fa fa-truck fa-5x\"></i>\n\n          </a>\n        </div>\n        <div>\n          <a href=\"\">\n            <!--<img src=\"assets/images/icon-forum.png\" alt=\"\" class=\"img-fluid\">\n            <span class=\"span\">Forum d'entraide</span>\n            -->\n            <i class=\"fas fa-map-marked-alt fa-5x\"></i>\n          </a>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"container\">\n    <ul id=\"menu-footer\">\n      <li><a href=\"#\">Informations légales</a></li>\n      <li><a href=\"\">Données personnelles</a></li>\n      <li><a href=\"\">&copy; Africa-Bonnes-Affaires</a></li>\n    </ul>\n  </div>\n</footer>\n"

/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/** Footer 2*/\n.footer {\n  background-color: #26445f;\n  padding: 1.5rem 0;\n  margin-bottom: -108px !important;\n  list-style: none; }\n.footer .border-footer {\n    padding: 1rem 0;\n    border-top: 1px solid white;\n    margin-bottom: 1.4rem;\n    font-size: 1.6rem; }\n.footer .map-africa {\n    width: 15%;\n    height: 15%;\n    margin: auto;\n    float: right; }\n@media (max-width: 575.98px) {\n    .footer .footer {\n      background-color: #26445f;\n      padding: 1.5rem 0;\n      margin-top: -88px !important;\n      list-style: none; } }\n.footer .icon-social-r {\n    font-size: 1.6rem; }\n.footer .title-footer {\n    font-weight: 600;\n    text-transform: uppercase;\n    border-bottom: 1px solid #a8b4c0 !important; }\n.footer .bar-nav {\n    display: flex;\n    padding: 1rem 0;\n    border-top: 1px solid white;\n    border-bottom: 1px solid white;\n    background-color: #26445f;\n    margin-top: -11px;\n    padding: 2px; }\n.footer a, .footer .icon {\n    color: white; }\n.footer ul li {\n    list-style: none;\n    margin-right: 23px; }\n.footer .btn-social {\n    border: 1px solid white;\n    border-radius: 50%; }\n#menu-footer {\n  padding-left: 0; }\n#menu-footer li {\n    display: inline; }\n"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());

/***Footer 2 */


/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<div class=\"col-md-6 d-lg-block position-fixed w-100 px-0\">\n  <div class=\"header\">\n    <a class=\"logo d-block\"  routerLink=\"\">\n      <img id=\"header-logo-mobile\" src=\"../../assets/logo_A-B-A.svg\" alt=\"logo site\" srcset=\"\">\n    </a>\n    <!--<label class=\"accross-text\">L'idée des bonnes affaires..</label>-->\n    <label class=\"menu-icon rounded-0\" for=\"menu-btn\"><span class=\"navicon\"></span></label>  \n    <input class=\"menu-btn\" type=\"checkbox\" id=\"menu-btn\"/>\n    <ul class=\"navigation  \">\n      <li class=\"search  mb-5\">\n        <a class=\"btn btn-danger \" id=\"butt-on\" href=\"#\">\n          <i class=\"fa fa-search fa-lg\" id=\"search-icon\"></i>\n        </a>\n        <input class=\"searchinger\" type='search' placeholder='votre recherche'/>\n      </li>\n    </ul>\n    <ul class=\"menu text-deco mr-4\">\n      <li>\n        <a routerLink=\"\">\n          <i class=\"fa fa-home fa-1x ml-lg-5 mr-3 d-lg-block\"></i>\n          <span class=\"before d-block\">Accueil</span>\n        </a>\n      </li>                     \n      <li>\n        <a routerLink=\"/panier\">\n          <i class=\"fas fa-heart fa-1x ml-lg-5 mr-3 d-lg-block \"></i>\n          <span class=\"before d-block\">Mes favoris{{panier.length}}</span>\n        </a>\n      </li>\n      <li>\n        <a routerLink=\"/user-account\">\n          <i class=\"fas fa-user-circle ml-lg-5 mr-3 d-lg-block\"></i>\n          <span class=\"before d-block\">Compte</span>\n        </a>\n      </li>\n      <div class=\"log-out d-flex align-items-end flex-column bd-highlight text-light mt-4\">\n        <a routerLink=\"/user-account\">\n          <p class=\"item-log-out p-2 bd-highlight\" *ngIf=\"isValid\" class=\"\">bonjour {{user.name}}</p>\n          <a class=\"cursor-log-out p-2 bd-highlight\" *ngIf=\"isValid\" (click)=\"logOut()\">Se déconnecter</a>\n        </a>\n      </div>\n    </ul>\n  </div>  \n</div>\n"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome {\n  display: flex;\n  justify-content: center;\n  position: relative;\n  top: 0;\n  color: #fffdfd;\n  font-family: 'hdoicons' !important;\n  right: 0; }\n\n/* * * SEARCH FORM SLIDE RIGHT STYLE 01 * * */\n\n.searchinger {\n  background: none repeat scroll 0 0 transparent;\n  border: 0 none;\n  font-family: 'Oswald', sans-serif;\n  font-size: 16px;\n  height: 34px;\n  padding: 0 0 0 10px;\n  position: absolute;\n  width: 0px;\n  transition: all .5s;\n  margin-left: -628px;\n  top: 23px;\n  background: none; }\n\n@media (max-width: 575.98px) {\n  .searchinger {\n    background: none repeat scroll 0 0 transparent;\n    font-family: 'Oswald', sans-serif;\n    font-size: 16px;\n    height: 28px;\n    padding: 0 0 0 10px;\n    position: fixed;\n    width: 28%;\n    transition: all .5s;\n    margin-left: 1px;\n    margin-top: 10px; } }\n\n#butt-on {\n  border-radius: 0;\n  border: 0 none;\n  height: 34px;\n  padding: 0;\n  margin-left: -664px;\n  width: 34px;\n  top: 13px;\n  position: relative; }\n\n@media (max-width: 575.98px) {\n  #butt-on {\n    border: 0 none;\n    height: 28px;\n    width: 36px;\n    padding: 0;\n    float: left;\n    margin-left: 10px; }\n  #butt-on:focus ~ .searchinger {\n    width: 200px;\n    background: #fff;\n    cursor: auto;\n    font-family: 'hdoicons' !important; }\n  .searchinger:focus {\n    width: 200px;\n    background: #bbb;\n    cursor: auto; } }\n\n#butt-on:focus ~ .searchinger {\n  width: 200px;\n  background: #fff;\n  cursor: auto;\n  font-family: 'hdoicons' !important; }\n\n.searchinger:focus {\n  width: 200px;\n  background: #bbb;\n  cursor: auto; }\n\n#search-icon {\n  margin: auto;\n  position: relative;\n  padding: 0px;\n  width: 116%;\n  top: 11px;\n  font-size: x-large; }\n\n@media (max-width: 575.98px) {\n  #search-icon {\n    margin: auto;\n    position: relative;\n    padding: 3px;\n    width: 111%;\n    top: 4px;\n    font-size: medium; } }\n\n/* * * BUTTON DEFAULT * * */\n\n.btn-danger {\n  color: #FFF;\n  background-color: #D9534F;\n  border-color: #D43F3A; }\n\n.btn {\n  display: inline-block;\n  margin-bottom: 0px;\n  font-weight: normal;\n  text-align: center;\n  vertical-align: middle;\n  cursor: pointer;\n  background-image: none;\n  border: 1px solid transparent;\n  white-space: nowrap;\n  padding: 6px 12px;\n  font-size: 14px;\n  line-height: 1.42857;\n  border-radius: 4px;\n  -moz-user-select: none; }\n\n/** example before \"|\" \n.before::before { \n  content: \"|\";\n  color: white;\n  margin-right: 25px;\n}\n@media (min-width: 768px) and (max-width: 1199.98px) { \n\n}\n@include media-breakpoint-only(xs) { ... }\n@include media-breakpoint-only(sm) { ... }\n@include media-breakpoint-only(md) { ... }\n@include media-breakpoint-only(lg) { ... }\n@include media-breakpoint-only(xl) { ... }\n// Extra small devices (portrait phones, less than 576px)\n@media (max-width: 575.98px) { ... }\n// Small devices (landscape phones, 576px and up)\n@media (min-width: 575.98px) and (max-width: 767.98px) { ... }\n// Extra small devices (portrait phones, less than 576px)\n@media (max-width: 575.98px) { ... }\n\n// Small devices (landscape phones, less than 768px)\n@media (max-width: 767.98px) { ... }\n\n// Medium devices (tablets, less than 992px)\n@media (max-width: 991.98px) { ... }\n\n// Large devices (desktops, less than 1200px)\n@media (max-width: 1199.98px) { ... } */\n\n.logo {\n  color: #f4f4f4 !important;\n  cursor: pointer;\n  position: relative;\n  font-style: normal;\n  margin-left: 584px;\n  font-family: 'hdoicons' !important;\n  font-size: 26px !important; }\n\n.logo a:hover {\n    color: #f4f4f4; }\n\n@media (min-width: 768px) and (max-width: 1199.98px) {\n  .logo {\n    cursor: pointer;\n    margin-left: 270px; } }\n\n@media (max-width: 575.98px) {\n  .logo {\n    top: px;\n    margin: auto;\n    width: 100%;\n    right: 23px; } }\n\n.text-deco li a {\n  color: red;\n  text-transform: uppercase;\n  text-decoration: none;\n  letter-spacing: 0.15em;\n  display: inline-block;\n  padding: 15px 20px;\n  position: relative; }\n\n.text-deco li a:after {\n  background: none repeat scroll 0 0 transparent;\n  bottom: 0;\n  content: \"\";\n  display: block;\n  height: 2px;\n  left: 50%;\n  position: absolute;\n  background: red #fff;\n  transition: width 0.3s ease 0s, left 0.3s ease 0s;\n  width: 0; }\n\n.text-deco li a:hover:after {\n  width: 100%;\n  left: 0; }\n\n.accross-text {\n  position: relative;\n  float: center;\n  color: #f4f4f4;\n  top: 95px;\n  top: 0;\n  z-index: 9999999977;\n  margin-left: 640px;\n  font-size: 1.7rem;\n  font-family: 'hdoicons' !important; }\n\na {\n  color: #ffffff; }\n\n/** menu hamburger*/\n\n.header {\n  background-color: #4267b2;\n  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);\n  position: fixed;\n  width: 100%;\n  height: 78px;\n  z-index: 3; }\n\n@media (min-width: 575.98px) and (max-width: 767.98px) {\n  .header {\n    box-shadow: 0 9px 4px 0 rgba(0, 0, 0, 0.15);\n    position: fixed;\n    width: 100%;\n    z-index: 3;\n    left: 0; } }\n\n@media (max-width: 575.98px) {\n  .header #header-logo-mobile {\n    width: 26%;\n    margin-left: 63px; } }\n\n.header ul {\n  margin: 0;\n  padding: 0;\n  list-style: none;\n  overflow: hidden; }\n\n.header li a {\n  font-size: .92857em;\n  font-weight: bolder;\n  display: block;\n  padding: 10px 20px;\n  -webkit-transition: color .3s ease;\n  border-right: 1px solid #f4f4f4;\n  transition: color .3s ease;\n  letter-spacing: .12em;\n  text-transform: uppercase;\n  color: #fff;\n  text-decoration: none;\n  margin-top: 10px; }\n\n@media (max-width: 575.98px) {\n  .header li a {\n    border-bottom: 2px solid #434f5f;\n    padding: 0px 19px;\n    height: 40px;\n    color: #ffff;\n    display: flex;\n    justify-content: flex-start;\n    align-items: center;\n    transition: opacity,.2s; } }\n\n.header li a:hover,\n.header .menu-btn:hover {\n  background-color: #d9534f; }\n\n.header .logo {\n  float: center;\n  position: relative;\n  font-size: 2em;\n  padding: 13px 0 3px 45px;\n  text-decoration: none;\n  height: 0;\n  top: -3px;\n  right: 230px;\n  z-index: 999999999; }\n\n@media (max-width: 575.98px) {\n  .header .logo {\n    top: 0;\n    right: 16px; } }\n\n/** menu     top: 0;\n    right: 16px; */\n\n.menu {\n  font-size: 1.5rem;\n  margin-top: -95px !important;\n  z-index: 919999999; }\n\n.header .menu {\n  clear: both;\n  max-height: 0;\n  transition: max-height .2s ease-out; }\n\n/* menu icon */\n\n.header .menu-icon {\n  cursor: pointer;\n  display: inline-block;\n  padding: 28px 20px;\n  position: absolute;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  top: 63px;\n  right: 50px;\n  color: #f4f4f4; }\n\n@media (max-width: 575.98px) {\n  .header .menu-icon {\n    position: relative;\n    top: -10px;\n    left: 0;\n    color: #fff;\n    z-index: 999999999;\n    background: none;\n    margin: auto; } }\n\n.header .menu-icon .navicon {\n  display: block;\n  height: 2px;\n  position: relative;\n  transition: background .2s ease-out;\n  width: 18px;\n  background: #f4f4f4; }\n\n.header .menu-icon .navicon:before,\n.header .menu-icon .navicon:after {\n  background: #f4f4f4;\n  content: '';\n  display: block;\n  height: 100%;\n  position: absolute;\n  transition: all .2s ease-out;\n  width: 100%; }\n\n.header .menu-icon .navicon:before {\n  top: 5px; }\n\n.header .menu-icon .navicon:after {\n  top: -5px; }\n\n/* menu btn */\n\n.header .menu-btn {\n  display: none; }\n\n.header .menu-btn:checked ~ .menu {\n  max-height: 240px; }\n\n@media (max-width: 575.98px) {\n  .header .menu-btn:checked ~ .menu {\n    margin: 0;\n    padding-bottom: 17px;\n    transition: all .3s ease;\n    background-color: #2e3641;\n    border-bottom: 1px solid #434f5f;\n    top: 15px;\n    position: relative;\n    width: 100%;\n    font-size: .92857em;\n    font-weight: bolder; } }\n\n.header .menu-btn:checked ~ .menu-icon .navicon {\n  background: transparent; }\n\n.header .menu-btn:checked ~ .menu-icon .navicon:before {\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg); }\n\n.header .menu-btn:checked ~ .menu-icon .navicon:after {\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg); }\n\n.header .menu-btn:checked ~ .menu-icon:not(.steps) .navicon:before,\n.header .menu-btn:checked ~ .menu-icon:not(.steps) .navicon:after {\n  top: 0; }\n\n/* 48em = 768px */\n\n@media (min-width: 48em) {\n  .header li {\n    float: left; }\n  .header li a {\n    padding: 20px 30px; }\n  .header .menu {\n    clear: none;\n    float: right;\n    max-height: none; }\n  .header .menu-icon {\n    display: none; } }\n\n/** end menu hamburger*/\n\n/** socials networks*/\n\n.fa, fa-cart-arrow-down, fa-1x {\n  color: white; }\n\n.navigation {\n  position: relative;\n  left: 100px;\n  display: flex;\n  justify-content: center;\n  z-index: -6;\n  top: -4px;\n  width: 103%; }\n\n@media (min-width: 575.98px) and (max-width: 767.98px) {\n  .navigation li > a {\n    display: none; } }\n\n.navigation a {\n  text-decoration: none;\n  display: block;\n  padding: 1em;\n  color: white; }\n\n.navigation a:hover {\n  background: #3f63ab; }\n\n@media (min-width: 1000px) {\n  .container {\n    max-width: 1000px; } }\n\n@media all and (max-width: 800px) {\n  .navigation {\n    justify-content: space-around; } }\n\n@media all and (max-width: 600px) {\n  .navigation {\n    display: flex;\n    position: relative;\n    left: 122px;\n    bottom: 50px;\n    background: none; }\n  .navigation a {\n    text-align: center;\n    padding: 10px;\n    border-top: 1px solid rgba(255, 255, 255, 0.3);\n    border-bottom: 1px solid rgba(0, 0, 0, 0.1); }\n  .navigation li:last-of-type a {\n    border-bottom: none; } }\n\n/**bare navigation header*/\n\n.icon-header {\n  position: relative;\n  float: right;\n  list-style: none;\n  margin: 34px;\n  font-size: 21px;\n  display: flex;\n  margin-top: -83px;\n  font-family: 'hdoicons' !important; }\n\n.icon-header a {\n  text-decoration: none;\n  display: block;\n  padding: 1em;\n  color: white; }\n\n.icon-header a:hover {\n  background: #d74b47; }\n\n@media (min-width: 1000px) {\n  .container {\n    max-width: 1000px; } }\n\n@media all and (max-width: 800px) {\n  .icon-header {\n    justify-content: space-around; } }\n\n@media all and (max-width: 600px) {\n  .icon-header {\n    flex-flow: column wrap;\n    padding: 0; }\n  .icon-headern a {\n    text-align: center;\n    padding: 10px;\n    border-top: 1px solid rgba(255, 255, 255, 0.3);\n    border-bottom: 1px solid rgba(0, 0, 0, 0.1); }\n  .icon-header li:last-of-type a {\n    border-bottom: none; } }\n\n.cursor-log-out {\n  cursor: pointer; }\n\n.log-out a {\n  display: block;\n  border-right: 1px solid #f4f4f4;\n  text-decoration: none;\n  font-size: .92857em;\n  font-weight: bolder;\n  text-transform: uppercase; }\n\n.log-out a:hover {\n  background-color: #d9534f !important;\n  color: #fff; }\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_panier_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/panier.service */ "./src/app/services/panier.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/e-com-store-api.service */ "./src/app/services/e-com-store-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(panierService, UserService, router, api) {
        var _this = this;
        this.panierService = panierService;
        this.UserService = UserService;
        this.router = router;
        this.api = api;
        this.user = {};
        this.isValid = false;
        this.UserService.user.subscribe(function (user) {
            _this.user = user;
            console.log(user);
            /**when does not have your interface on creates an object with a hack ["name"] */
            if (user["name"])
                _this.isValid = true;
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.panier = this.panierService.getAllProduits();
    };
    /**method logout of application */
    HeaderComponent.prototype.logOut = function () {
        this.user = null;
        this.UserService.setUser({});
        this.isValid = false;
        this.api.setToken("");
        this.api.setLogged(false);
        this.router.navigate(['/login']);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_services_panier_service__WEBPACK_IMPORTED_MODULE_1__["PanierService"], _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_4__["EComStoreApiService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/is-sign-in.guard.ts":
/*!*************************************!*\
  !*** ./src/app/is-sign-in.guard.ts ***!
  \*************************************/
/*! exports provided: IsSignInGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsSignInGuard", function() { return IsSignInGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/e-com-store-api.service */ "./src/app/services/e-com-store-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IsSignInGuard = /** @class */ (function () {
    function IsSignInGuard(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    IsSignInGuard.prototype.canActivate = function () {
        var isLogged = this.userService.getLogged();
        if (!isLogged) {
            this.router.navigate([""]);
        }
        return isLogged;
    };
    IsSignInGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_2__["EComStoreApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], IsSignInGuard);
    return IsSignInGuard;
}());



/***/ }),

/***/ "./src/app/not-found/not-found.component.html":
/*!****************************************************!*\
  !*** ./src/app/not-found/not-found.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>\n    404 Not found!\n</h2>\n"

/***/ }),

/***/ "./src/app/not-found/not-found.component.scss":
/*!****************************************************!*\
  !*** ./src/app/not-found/not-found.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/*!**************************************************!*\
  !*** ./src/app/not-found/not-found.component.ts ***!
  \**************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-not-found',
            template: __webpack_require__(/*! ./not-found.component.html */ "./src/app/not-found/not-found.component.html"),
            styles: [__webpack_require__(/*! ./not-found.component.scss */ "./src/app/not-found/not-found.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/panier/panier.component.html":
/*!**********************************************!*\
  !*** ./src/app/panier/panier.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<h2 class=\"welcome\">votre panier {{user.name}}</h2>\n<!--<select name=\"\" id=\"\" disabled=\"disabled\"></select>-->\n<div [ngClass]=\"'container product'\">\n    <div [ngClass]=\"'row productBox'\" *ngFor=\"let produit of panier; index as i\">\n      <div class=\"col- shadow\">\n        <div>\n          <h4>\n            <a [routerLink] = \"['/panier', produit.id]\">{{produit.name}}</a>\n          </h4>\n          <img  class=\"img-fluid cursor img\" [src]=\"produit.images | safe\" alt=\"images des produits\">\n          <div class=\"d-flex justify-content-center\">\n            <h5>Prix:{{produit.price}}</h5>\n          </div>\n          <!--<div class=\"d-flex justify-content-center\">\n            <p>{{produit.description}}</p>             \n          </div>-->  \n          <div class=\"d-flex justify-content-center\">\n            <p>{{produit.marque}}</p>              \n          </div>\n        </div>\n        <div class=\"d-flex justify-content-center\">\n          <button class=\"color cursor \" type=\"button\" (click)=\"deleteProduitInPanier(i)\">supprimer</button>\n        </div>       \n      </div>\n    </div>\n  </div>\n\n"

/***/ }),

/***/ "./src/app/panier/panier.component.scss":
/*!**********************************************!*\
  !*** ./src/app/panier/panier.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contenair {\n  display: flex;\n  justify-content: space-around;\n  flex-wrap: wrap; }\n\n.welcome {\n  position: relative;\n  top: 66px;\n  font-size: 16px;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n  left: 84%;\n  color: #4167b2; }\n\n.productBox {\n  display: inline-flex;\n  background-color: #7e7879;\n  color: white;\n  margin-top: 80px;\n  font-size: 17px;\n  flex-wrap: wrap;\n  border-radius: 5px;\n  margin: 100px 2px 0;\n  box-shadow: inset 0 0 10px #b1a6a6; }\n\n.shadow:hover {\n  box-shadow: inset 0 0 10px #000000; }\n\n.cursor {\n  cursor: pointer; }\n\n.color {\n  color: white;\n  background-color: #096ec8;\n  border-radius: 1.25rem; }\n\n.color:hover {\n  color: #ffffff;\n  background-color: #099e7d; }\n\na {\n  color: white;\n  cursor: pointer;\n  list-style: none;\n  text-align: center; }\n\n.img {\n  width: 200px; }\n"

/***/ }),

/***/ "./src/app/panier/panier.component.ts":
/*!********************************************!*\
  !*** ./src/app/panier/panier.component.ts ***!
  \********************************************/
/*! exports provided: PanierComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanierComponent", function() { return PanierComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_panier_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/panier.service */ "./src/app/services/panier.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { Produit } from 'src/app/entities/produit';
var PanierComponent = /** @class */ (function () {
    function PanierComponent(panierService, UserService) {
        this.panierService = panierService;
        this.UserService = UserService;
    }
    PanierComponent.prototype.ngOnInit = function () {
        // this.dataService.getAll().subscribe(value => this.conproduits = value); 
        this.panier = this.panierService.getAllProduits();
        console.log(this.panier);
        if (this.UserService.user) {
            this.user = this.UserService.user;
        }
    };
    /***method delete product in basket/favoris */
    PanierComponent.prototype.deleteProduitInPanier = function (id) {
        this.panierService.deleteProduit(id);
    };
    PanierComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-panier',
            template: __webpack_require__(/*! ./panier.component.html */ "./src/app/panier/panier.component.html"),
            styles: [__webpack_require__(/*! ./panier.component.scss */ "./src/app/panier/panier.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_panier_service__WEBPACK_IMPORTED_MODULE_1__["PanierService"], _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], PanierComponent);
    return PanierComponent;
}());



/***/ }),

/***/ "./src/app/pipes/safe.pipe.ts":
/*!************************************!*\
  !*** ./src/app/pipes/safe.pipe.ts ***!
  \************************************/
/*! exports provided: SafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return SafePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SafePipe = /** @class */ (function () {
    /**Le safe prend l'url en base  64 pour la déclarer comme sans danger (safe*/
    /** The safe takes the url in base 64 to declare it as safe */
    function SafePipe(domsatinizer) {
        this.domsatinizer = domsatinizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.domsatinizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'safe'
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"]])
    ], SafePipe);
    return SafePipe;
}());



/***/ }),

/***/ "./src/app/produit-fiche/produit-fiche.component.html":
/*!************************************************************!*\
  !*** ./src/app/produit-fiche/produit-fiche.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container product\">\n  <div class=\"row productBox shadow-block-product\">\n    <div class=\"col- shadow\">\n      <div>\n        <h4 class=\"d-flex justify-content-center\">\n          <a [routerLink] = \"['/produit-fiche', produit.id]\">{{produit.name}}</a>\n        </h4>\n        <img  class=\"img-fluid img-thumbnail cursor img\" [src]=\"produit.images | safe\" alt=\"images des produits\">              \n        <!--<div class=\"d-flex justify-content-center\">\n          <a class=\"phone-button-tel text-position\" href=\"http://\">\n            <i class=\"fas fa-mobile-alt fa-1x phone-tel\"></i>\n            Téléphone {{produit.phoneNumber}}\n          </a>\n        </div>\n        <div class=\"d-flex justify-content-center\">\n            <a [routerLink] = \"['/produit-list', produit.id]\" class=\"phone-button-email text-position\" href=\"http://\">\n              <i class=\"fas fa-envelope fa-1x phone-email\"></i>\n              Email {{produit.email}}\n            </a>\n        </div>-->\n        <div class=\"d-flex justify-content-center\">\n          <h5 class=\"text-price text-position\" >Prix:{{produit.price}}</h5>\n        </div>\n        <div class=\"d-flex justify-content-center\">\n          <a (click)=\"goToFiche(produit.id)\" data-toggle=\"tooltip\" title=\"Produit d'occasion vendu avec la garantie!\">\n            <p class=\"text-position\">Description :<br>{{produit.description}}</p>\n          </a>\n        </div>\n        <div class=\"d-flex justify-content-center\">\n          <p [routerLink] = \"['/produit-fiche', produit.id]\">{{produit.category}}</p>\n        </div>\n        <div class=\"d-flex justify-content-center\">\n          <p>{{produit.marque}}</p> \n        </div>\n      </div>\n      <div class=\"d-flex justify-content-center\">\n        <button class=\"color cursor mb-4\" type=\"button\" (click)=\"addProduitInPanier(produit)\">\n          Ajouter aux favoris\n        </button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/produit-fiche/produit-fiche.component.scss":
/*!************************************************************!*\
  !*** ./src/app/produit-fiche/produit-fiche.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".aside {\n  display: flex;\n  justify-content: space-around;\n  flex-direction: row;\n  align-items: center; }\n\nimg {\n  width: 100px; }\n\n.button:hover {\n  background: #c10a27; }\n\n.contenair {\n  display: flex;\n  justify-content: space-around;\n  flex-wrap: wrap; }\n\n@media (min-width: 1200px) {\n  .container {\n    max-width: 1063px; } }\n\n.shadow:hover {\n  box-shadow: inset 0 0 10px #000000; }\n\n.product {\n  margin-top: -2px;\n  margin-left: 24px; }\n\n.productBox {\n  display: inline-flex;\n  background-color: #7e7879;\n  color: white;\n  margin-top: 70px;\n  font-size: 16px;\n  flex-wrap: wrap; }\n\n.cursor {\n  cursor: pointer; }\n\n.color {\n  color: white;\n  background-color: #c10a27;\n  border-radius: 1.25rem; }\n\n.color:hover {\n  color: #ffffff;\n  background-color: #35e4e6; }\n\np {\n  width: 50%;\n  margin: auto;\n  margin-bottom: 5rem; }\n\na {\n  color: white;\n  cursor: pointer;\n  list-style: none;\n  text-align: center; }\n\n.img {\n  width: 250px; }\n\n.text-position {\n  position: relative;\n  font-family: ' hdoicons' !important;\n  font-size: 1.8rem;\n  margin: auto;\n  float: right;\n  left: 400px;\n  color: #000000;\n  top: -155px; }\n\n.shadow {\n  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n  background-color: #4183d7;\n  font-family: 'hdoicons' !important; }\n"

/***/ }),

/***/ "./src/app/produit-fiche/produit-fiche.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/produit-fiche/produit-fiche.component.ts ***!
  \**********************************************************/
/*! exports provided: ProduitFicheComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProduitFicheComponent", function() { return ProduitFicheComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_produit_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/produit.service */ "./src/app/services/produit.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_panier_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/panier.service */ "./src/app/services/panier.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProduitFicheComponent = /** @class */ (function () {
    function ProduitFicheComponent(service, panier, route, UserService) {
        var _this = this;
        this.service = service;
        this.panier = panier;
        this.route = route;
        this.UserService = UserService;
        this.produit = { category: "", description: "", id: 0, imageURI: "", marque: "", name: "", price: "" };
        this.service.getProduit(this.route.snapshot.params.id).subscribe(function (value) { _this.produit = value; });
    }
    ProduitFicheComponent.prototype.ngOnInit = function () {
        if (this.UserService.user) {
            this.user = this.UserService.user;
            console.log(this.user);
        }
    };
    /***add a product to the cart */
    ProduitFicheComponent.prototype.addProduitInPanier = function (produit) {
        this.panier.addProduit(produit);
        console.log(this.panier.panier);
    };
    ProduitFicheComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-produit-fiche',
            template: __webpack_require__(/*! ./produit-fiche.component.html */ "./src/app/produit-fiche/produit-fiche.component.html"),
            styles: [__webpack_require__(/*! ./produit-fiche.component.scss */ "./src/app/produit-fiche/produit-fiche.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_produit_service__WEBPACK_IMPORTED_MODULE_1__["ProduitService"], src_app_services_panier_service__WEBPACK_IMPORTED_MODULE_4__["PanierService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
    ], ProduitFicheComponent);
    return ProduitFicheComponent;
}());



/***/ }),

/***/ "./src/app/produit-list/produit-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/produit-list/produit-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!--slides publicity-->\n<base href=\"https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/\">\n<div id=\"slider\">\n  <figure>\n  <img src=\"https://images.unsplash.com/photo-1419064642531-e575728395f2?crop=entropy&fit=crop&fm=jpg&h=400&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1400\" alt=\"Chania\">\n  <img src=\"https://images.unsplash.com/photo-1445280471656-618bf9abcfe0?crop=entropy&fit=crop&fm=jpg&h=400&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1400\" alt=\"Chania\" alt>\n  <img src=\"https://images.unsplash.com/photo-1419064642531-e575728395f2?crop=entropy&fit=crop&fm=jpg&h=400&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1400\" alt=\"Chania\">\n  <img src=\"https://images.unsplash.com/photo-1445280471656-618bf9abcfe0?crop=entropy&fit=crop&fm=jpg&h=400&ixjsv=2.1.0&ixlib=rb-0.3.5&q=80&w=1400\" alt=\"Flower\">\n  \n  </figure>\n</div>\n<!--div block z-index-->\n<div class=\"block-forward\">\n    <div [ngClass]=\"{'bg-white': themeScheme == 'light','bg-dark': themeScheme == 'dark'}\" class=\"floated-social shadow-lg d-none d-lg-block\">\n      <a href=\"https://www.facebook.com/groups/africabonnesaffaires/\"><i class=\"fab fa-facebook-f\"></i></a>\n      <a href=\"#\"><i class=\"fab fa-twitter\"></i></a>\n      <a href=\"#\"><i class=\"fab fa-instagram\"></i></a>\n    </div>\n\n    <div class=\"block-annonce\">\n      <button type=\"button\" class=\"btn btn-primary btn-lg btn-block btn-annonce\" (click)=\"submit()\">\n        <a [routerLink] = \"['/ad-page']\">\n          <i class=\"far fa-plus-square fa-1x\"></i>\n          Déposer votre annonce\n        </a>\n      </button>\n    </div>\n    <!--Annonces à la une-->\n    <div class=\" welcome d-none d-lg-block\">\n      <span>Annonces À LA UNE</span>\n      <div class=\"container-fluid d-none d-lg-block img-la-une\">\n          <img class=\"img-fluid\" src=\"../../assets/montre-femme.jpg\" alt=\"images annonces à la une\" srcset=\"\">\n          <!--<img class=\"img-fluid\" src=\"../../assets/montre-festina-homme.jpg\" alt=\"images annonces à la une\" srcset=\"\">-->\n      </div>\n    </div>\n\n    <div class=\"container products\">\n      <div class=\"row\">\n        <div [ngClass]=\"'product w-100'\" >\n            <div class=\"shadow-block-product\" [ngClass]=\"'productBox'\" *ngFor=\"let produit of produits \">\n             \n              <div class=\"container  shadow\">\n                <div class=\"#\">\n                  <div class=\"container text-color \">\n                    <h4 class=\"d-flex justify-content-center  title-product\">\n                      <a [routerLink] = \"['/produit-list', produit.id]\">{{produit.name}}</a>\n                    </h4>\n                    <div class=\"bg-img-products\">\n                     <img  class=\"im-fluid img-thumbnail zoom cursor img-products\"  [src]=\"produit.images | safe\" alt=\"images des produits\">              \n                    </div> \n                    <div class=\"font-category d-flex justify-content-center\">\n                      <p [routerLink] = \"['/produit-list', produit.id]\">{{produit.category}}</p>\n                    </div>\n                    <div class=\"d-flex justify-content-center\">\n                      <p>{{produit.marque}}</p> \n                    </div>\n                  </div>\n                  <div class=\"row d-flex justify-content-center\">\n                    <button class=\"color cursor mb-4\" type=\"button\" (click)=\"addProduitInPanier(produit)\">\n                      Ajouter aux favoris\n                      <i class=\"fas fa-heart fa-1x ml-5 icon-favoris cursor\"></i>\n                    </button>\n                  </div>\n                </div>\n                \n                <div class=\"container text-color mt-5\">\n                  <div class=\"d-flex justify-content-center \">\n                    <div class=\"item2\">\n                      <a [routerLink] = \"['/produit-list', produit.id]\" class=\"phone-button-tel \" href=\"http://\">\n                        <i class=\"fas fa-mobile-alt fa-1x phone-tel\"></i>\n                        Tél: {{produit.phoneNumber}}\n                      </a>\n                    </div>\n                    <div class=\"item2\">\n                        <a class=\"row\"[routerLink] = \"['/produit-list', produit.id]\" class=\"phone-button-email \" href=\"http://\">\n                          <i class=\"fas fa-envelope fa-1x phone-email\"></i>\n                          Mail: {{produit.email}}\n                        </a>\n                    </div>\n                  </div>\n                  <div class=\"d-flex justify-content-center text-color mt-5\">\n                    <h2 class=\"color-price\">Prix:{{produit.price}}</h2>\n                  </div>\n                  <div class=\"d-flex justify-content-center text-color\">\n                    <a (click)=\"goToFiche(produit.id)\" data-toggle=\"tooltip\" title=\"Produit d'occasion vendu avec la garantie!\">\n                      <p>Voir détails produit...<br></p>\n                    </a>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n      </div>\n    </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/produit-list/produit-list.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/produit-list/produit-list.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\n.shadow {\n  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n  background-color: #fff;\n  font-family: 'hdoicons' !important;\n  display: flex;\n  align-items: left;\n  border-color: none; }\n.welcome {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  padding: 0 15px 20px 13px;\n  align-items: center;\n  position: relative;\n  top: -6px;\n  color: #d9534f;\n  cursor: pointer;\n  font-size: 30px;\n  font-weight: bold;\n  font-family: 'hdoicons' !important;\n  float: right;\n  right: 45px;\n  background-color: #ebeaed;\n  height: 350px;\n  width: 19%;\n  border-radius: 5px;\n  box-shadow: rgba(121, 121, 121, 0.35) 1px 10px 20px; }\n.welcome span {\n    font-size: 23px; }\n.welcome .img-la-une {\n    display: flex;\n    justify-content: center;\n    flex-direction: column; }\n.shadow:hover {\n  box-shadow: inset 0 0 10px #000000; }\n/**\n@media (min-width: 768px) and (max-width: 1199.98px) { ... }\n@include media-breakpoint-only(xs) { ... }\n@include media-breakpoint-only(sm) { ... }\n@include media-breakpoint-only(md) { ... }\n@include media-breakpoint-only(lg) { ... }\n@include media-breakpoint-only(xl) { ... }\n// Extra small devices (portrait phones, less than 576px)\n@media (max-width: 575.98px) { ... }\n// Small devices (landscape phones, 576px and up)\n@media (min-width: 575.98px) and (max-width: 767.98px) { ... }\n@media (min-width: 48em)  \n*/\n@media (min-width: 575.98px) and (max-width: 767.98px) {\n  .products {\n    display: flex;\n    align-items: center; } }\n.product {\n  margin-top: 30px;\n  margin-left: 38px; }\n.productBox {\n  display: flex;\n  background-color: #fff;\n  color: white;\n  font-size: 16px;\n  border: 1px solid #fff;\n  box-shadow: 8px -40px 134px #fff;\n  padding: 0;\n  margin: 3px 50px 0 38px;\n  width: 100%;\n  margin-left: -44px; }\n.cursor {\n  cursor: pointer; }\n.text-price {\n  font-size: 2rem;\n  margin-top: 4px; }\n.color {\n  padding: 0.7em 0.7em;\n  /* color: white; */\n  background-color: #096ec8;\n  border-radius: 24px;\n  margin-left: 3px;\n  border: none; }\n@media (max-width: 575.98px) {\n  .color {\n    padding: 0px 12px 3px 25px;\n    background: none;\n    border-radius: 40px;\n    left: 28px;\n    border: none;\n    height: 31px;\n    font-weight: bold;\n    font-size: 11px;\n    position: relative;\n    text-align: left;\n    bottom: -27px;\n    position: relative; } }\n@media (max-width: 575.98px) {\n  .icon-favoris {\n    color: #f57d4e;\n    border-color: black;\n    position: relative;\n    left: -10px;\n    bottom: 35px;\n    z-index: 100;\n    font-size: 18px;\n    cursor: pointer; } }\n.color:hover {\n  color: #ffffff;\n  background-color: #099e7d; }\n.title-product a {\n  color: #000000;\n  font-weight: 500;\n  text-decoration: none; }\na {\n  color: #000000;\n  cursor: pointer;\n  list-style: none;\n  text-align: center;\n  font-size: 24px; }\n.bg-img-products {\n  background-color: #0b7f83 !important;\n  border-color: yellow;\n  z-index: 9999999999; }\n.bg-img-products .img-products {\n    width: 210px; }\n/** zom images*/\n.zoom {\n  transition: all 0.35s ease-in-out;\n  cursor: zoom-in; }\n.zoom:hover,\n.zoom:active,\n.zoom:focus {\n  /**adjust scale to desired size, \nadd browser prefixes**/\n  -webkit-transform: scale(1.4);\n  transform: scale(1.4);\n  position: relative;\n  z-index: 100; }\n/**Carrousel homepage*/\n/**slide à la UNE logo clients*/\n.carousel-sli {\n  position: relative !important;\n  width: 60%;\n  margin: auto;\n  top: 90px; }\n.block-forward {\n  position: relative;\n  top: -130px;\n  z-index: 2; }\n@media (max-width: 575.98px) {\n  .block-forward {\n    position: relative;\n    z-index: 2; } }\n.btn-annonce {\n  position: relative;\n  background-color: #4183d8;\n  border-color: #4183d8;\n  font-family: 'hdoicons' !important;\n  width: 915px !important;\n  left: -119px !important;\n  margin: auto;\n  font-size: 1.6rem;\n  top: 36px;\n  border-radius: 0; }\n.btn-annonce a {\n    color: #fff;\n    text-transform: uppercase;\n    text-decoration: none;\n    letter-spacing: 0.1em;\n    display: inline-block;\n    padding: 15px 20px;\n    position: relative; }\n.btn-annonce a:after {\n    background: none repeat scroll 0 0 transparent;\n    bottom: 0;\n    content: \"\";\n    display: block;\n    height: 2px;\n    position: absolute;\n    background: #fff;\n    transition: width 0.3s ease 0s, left 0.3s ease 0s;\n    width: 0; }\n.btn-annonce a:hover:after {\n    width: 78%;\n    left: 64px; }\n.btn-annonce a {\n    font-size: 2.3rem;\n    font-weight: bolder;\n    color: #fff;\n    text-decoration: none; }\n.block-annonce {\n  position: relative;\n  align-items: center; }\n.shadow-block-product {\n  top: 100px;\n  border-radius: .5rem; }\n.phone-button-tel {\n  font-family: 'hdoicons' !important;\n  color: #fff;\n  border-radius: 5px;\n  align-items: center;\n  font-size: 1.9rem; }\n.phone-button-tel .phone-tel {\n    color: #ffffff;\n    padding: 10px; }\n.phone-button-tel a {\n    color: #2f3541; }\n.phone-button-email {\n  font-family: 'hdoicons' !important;\n  color: #fff;\n  border-radius: 5px;\n  font-size: 1.9rem;\n  align-items: center; }\n.phone-button-email .phone-email {\n    color: #ffffff;\n    padding: 10px; }\n/**carousel */\n@-webkit-keyframes slidy {\n  0% {\n    left: 0%; }\n  20% {\n    left: 0%; }\n  25% {\n    left: -100%; }\n  45% {\n    left: -100%; }\n  50% {\n    left: -200%; }\n  70% {\n    left: -200%; }\n  75% {\n    left: -300%; }\n  95% {\n    left: -300%; }\n  100% {\n    left: -400%; } }\n@keyframes slidy {\n  0% {\n    left: 0%; }\n  20% {\n    left: 0%; }\n  25% {\n    left: -100%; }\n  45% {\n    left: -100%; }\n  50% {\n    left: -200%; }\n  70% {\n    left: -200%; }\n  75% {\n    left: -300%; }\n  95% {\n    left: -300%; }\n  100% {\n    left: -400%; } }\ndiv#slider {\n  overflow: hidden; }\ndiv#slider figure img {\n  width: 20%;\n  float: left; }\ndiv#slider figure {\n  position: relative;\n  width: 500%;\n  margin: 0;\n  left: 0;\n  text-align: left;\n  font-size: 0;\n  -webkit-animation: 30s slidy infinite;\n          animation: 30s slidy infinite; }\n/** toggle infos contact*/\n.item2 {\n  padding: 0.3em 0em;\n  border-style: solid;\n  border-color: coral;\n  border-radius: 64px;\n  margin-left: 3px;\n  width: 38%;\n  float: right;\n  display: flex;\n  flex-wrap: wrap; }\n.item2:hover {\n    background-color: coral; }\n.item2 a {\n    color: #000000;\n    text-decoration: none; }\n.item2 a:hover {\n      color: #ffffff; }\n@media (max-width: 575.98px) {\n  .item2 {\n    font-size: 2px;\n    font-weight: 100;\n    border: none; } }\n.text-color h4, h2, p {\n  color: #000000;\n  font-weight: inherit;\n  margin-top: 12px; }\n@media (max-width: 575.98px) {\n  .text-color h4, h2, p {\n    position: relative;\n    font-size: 15px;\n    font-weight: 700; } }\n@media (max-width: 575.98px) {\n  .text-color {\n    position: relative;\n    top: -8px;\n    float: left;\n    display: flex;\n    flex-wrap: wrap-reverse; } }\n.font-category {\n  font-weight: bold; }\n.color-price {\n  color: #f57d4e;\n  font-weight: 700; }\n@media (max-width: 575.98px) {\n  .color-price {\n    color: #f57d4e;\n    font-weight: 700;\n    text-align: center;\n    position: relative;\n    left: 100px; } }\n/**end toggle inofos contact*/\n/**** For a future floated menu for social network *****/\n.floated-social {\n  transition: opacity 1s ease-in-out;\n  opacity: 1;\n  position: fixed;\n  z-index: 6000;\n  top: 16em;\n  left: 0;\n  padding: 7.5px 7.5px 7.5px 7.5px;\n  border-radius: 0 5px 5px 0;\n  background-color: #888 !important;\n  border-color: #4183d8 !important; }\n.floated-social a {\n    -webkit-text-decoration-line: none;\n            text-decoration-line: none;\n    color: #ffffff; }\n.floated-social i {\n    font-size: 1.5em;\n    align-content: center;\n    text-align: center;\n    margin-bottom: 5px; }\n.floated-social i.fab {\n      display: block;\n      border-radius: 60px;\n      box-shadow: 0px 0px 2px #888;\n      padding-top: 0.30em;\n      transition: all 0.3s ease;\n      width: 45px;\n      height: 45px; }\n.floated-social i.fab:hover {\n        background-color: #ffff; }\n.floated-social i.fa-twitter {\n      padding-left: 1px; }\n.floated-social i.fa-twitter:hover {\n        color: #1da1f2; }\n.floated-social i.fa-facebook-f {\n      margin-bottom: 0px; }\n.floated-social i.fa-facebook-f:hover {\n        color: #4167b2; }\n.floated-social i.fa-instagram:hover {\n      color: #d2594f; }\n"

/***/ }),

/***/ "./src/app/produit-list/produit-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/produit-list/produit-list.component.ts ***!
  \********************************************************/
/*! exports provided: ProduitListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProduitListComponent", function() { return ProduitListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_produit_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/produit.service */ "./src/app/services/produit.service.ts");
/* harmony import */ var src_app_services_panier_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/panier.service */ "./src/app/services/panier.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProduitListComponent = /** @class */ (function () {
    function ProduitListComponent(service, UserService, panier, router) {
        this.service = service;
        this.UserService = UserService;
        this.panier = panier;
        this.router = router;
        this.user = {};
    }
    ProduitListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getAllProduit().subscribe(function (value) {
            _this.produits = value;
            console.log(value);
        });
        if (this.UserService.user) {
            this.user = this.UserService.user;
            console.log(this.user);
        }
    };
    ProduitListComponent.prototype.addProduitInPanier = function (produit) {
        this.panier.addProduit(produit);
        // console.log(this.panier.panier)
    };
    ProduitListComponent.prototype.goToFiche = function (id) {
        this.router.navigate(["produit-fiche", id]);
    };
    ProduitListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-produit-list',
            template: __webpack_require__(/*! ./produit-list.component.html */ "./src/app/produit-list/produit-list.component.html"),
            styles: [__webpack_require__(/*! ./produit-list.component.scss */ "./src/app/produit-list/produit-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_produit_service__WEBPACK_IMPORTED_MODULE_1__["ProduitService"], _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], src_app_services_panier_service__WEBPACK_IMPORTED_MODULE_2__["PanierService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ProduitListComponent);
    return ProduitListComponent;
}());



/***/ }),

/***/ "./src/app/services/e-com-store-api.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/e-com-store-api.service.ts ***!
  \*****************************************************/
/*! exports provided: EComStoreApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EComStoreApiService", function() { return EComStoreApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EComStoreApiService = /** @class */ (function () {
    function EComStoreApiService(http) {
        this.http = http;
        this.apiDomain = "http://137.74.113.253:3000";
        this.isLogged = false;
        this.isloggedValidator = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
    }
    EComStoreApiService.prototype.getToken = function () {
        return this.token;
    };
    /**(Set token) send a token to gain access */
    EComStoreApiService.prototype.setHttpOptions = function () {
        var head = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json',
            'Authorization': this.getToken() });
        var httpOptions = {
            headers: head
        };
        return httpOptions;
    };
    EComStoreApiService.prototype.setToken = function (token) {
        console.log(token);
        localStorage.setItem('nom du token', JSON.stringify(token));
        this.token = token;
    };
    /**New user login */
    EComStoreApiService.prototype.newUser = function (user) {
        return this.http.post(this.apiDomain + "/auth/register", user);
    };
    EComStoreApiService.prototype.login = function (user) {
        console.log("user:", user);
        return this.http.post(this.apiDomain + "/auth/login", user);
    };
    EComStoreApiService.prototype.setLogged = function (value) {
        this.isLogged = value;
    };
    EComStoreApiService.prototype.getLogged = function () {
        return this.isLogged;
    };
    /**add a product and save it */
    EComStoreApiService.prototype.addProduct = function (product) {
        return this.http.post(this.apiDomain + "/product/register", product);
    };
    /**(Get user byId): Recover a user by his Id **/
    EComStoreApiService.prototype.getById = function (id) {
        return this.http.get(this.apiDomain + "/users/" + id, this.setHttpOptions());
    };
    EComStoreApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EComStoreApiService);
    return EComStoreApiService;
}());



/***/ }),

/***/ "./src/app/services/panier.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/panier.service.ts ***!
  \********************************************/
/*! exports provided: PanierService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanierService", function() { return PanierService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PanierService = /** @class */ (function () {
    function PanierService() {
        /**verifier si il y'a rien dans le localstorage on le
         *  réinitialise en tableau vide et on le sauvegarde */
        /**intialiser le panier qui est un ensemble de produits */
        this.panier = [];
        if (this.read('panier') === null) {
            this.save();
        }
        /**S'il y'a quelque chose dans le localstorage,
         *  on le récupère et on le met dans le panier */
        this.panier = this.read('panier');
    }
    /** Récupérer touts les produits d'un panier*/
    PanierService.prototype.getAllProduits = function () {
        return this.panier;
    };
    PanierService.prototype.addProduit = function (produit) {
        this.panier.push(produit);
        this.save();
        return this.panier;
    };
    /**Supprimer un produit à partir de son id */
    PanierService.prototype.deleteProduit = function (id) {
        this.panier.splice(id, 1);
        this.save();
        return this.panier;
    };
    /**Vider le panier */
    PanierService.prototype.clearPanier = function () {
        this.panier = [];
        this.save();
        return this.panier;
    };
    /**Sauvegarder le panier dans le localStorage */
    PanierService.prototype.save = function () {
        var json = JSON.stringify(this.panier);
        ;
        localStorage.setItem('panier', json);
    };
    /**Lire un panier depuis le localStorage */
    PanierService.prototype.read = function (itemName) {
        var json = localStorage.getItem(itemName);
        return JSON.parse(json);
    };
    PanierService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            /**singleton pour extensier la class du service une seul fois */
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], PanierService);
    return PanierService;
}());



/***/ }),

/***/ "./src/app/services/produit.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/produit.service.ts ***!
  \*********************************************/
/*! exports provided: ProduitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProduitService", function() { return ProduitService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProduitService = /** @class */ (function () {
    function ProduitService(httpClient) {
        this.httpClient = httpClient;
        this.apiDomain = "http://137.74.113.253:3000";
    }
    /** Method to recover a product by the id */
    ProduitService.prototype.getAllProduit = function () {
        var _this = this;
        return this.httpClient.get(this.apiDomain + "/products").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (value) {
            console.log(value);
            _this.produits = value;
        }));
    };
    /** Method to recover a product by the id */
    ProduitService.prototype.getProduit = function (id) {
        return this.httpClient.get(this.apiDomain + "/products/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (value) {
            console.log(value);
            // this.localStorageService.setItem(`produit.${id}`, value);     
        }));
    };
    /**Method to recover all products */
    ProduitService.prototype.getProduits = function () {
        return this.produits;
        console.log("get current produits", this.produits);
    };
    /** Method to recover a product by the id */
    ProduitService.prototype.setUser = function (produits) {
        this.produits = produits;
        console.log("current produits", this.produits);
    };
    ProduitService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProduitService);
    return ProduitService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = /** @class */ (function () {
    function UserService(httpClient) {
        this.httpClient = httpClient;
        //user:Object;
        this.user = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
    }
    UserService.prototype.getUser = function () {
        return this.user.getValue();
    };
    UserService.prototype.setUser = function (user) {
        //this.user = user;
        this.user.next(user);
        console.log("current user", this.user);
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/user-account/user-account.component.html":
/*!**********************************************************!*\
  !*** ./src/app/user-account/user-account.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n  <div class=\"user\">\n      <h1>S'identifier</h1>\n      <form>\n        <fieldset>\n          <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"Email \" (change)=\"check($event);\">\n          <input type=\"password\" id=\"password\" name=\"password\" placeholder=\"Mot de passe\" (change)=\"check2($event);\">\n          <!--<div id=\"warning\"></div>-->\n          <button id=\"button-connect\"(click)=\"submit()\" type=\"button\">Connexion</button>\n          <button  (click)=\"submit()\" type=\"button\"><a class=\"sign-up\" [routerLink] = \"['/user-sign-up']\">Créer mon compte</a></button>\n        </fieldset>\n        <p>En cliquant sur le bouton S'inscrire, vous acceptez notre <a href=\"#\">Terme &amp; Conditions</a>, & <a href=\"#\">Politique de confidentialité</a>.</p>\n      </form>\n  </div>\n</section>\n "

/***/ }),

/***/ "./src/app/user-account/user-account.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/user-account/user-account.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(\"https://fonts.googleapis.com/css?family=Open+Sans\");\n/* FONTS */\n/* BODY */\nsection {\n  background: #f6f6f6;\n  margin: 0;\n  width: 100%;\n  text-align: center;\n  font-family: 'Open Sans', sans-serif;\n  font-size: 10px;\n  padding-top: 125px; }\n/* FORM */\n.user {\n  width: 290px;\n  background: #ffffff;\n  margin: 20px auto 20px auto;\n  box-shadow: 1px 1px 3px 3px #d5cdcd;\n  border-radius: 4px;\n  text-align: center;\n  font-family: 'Open Sans', sans-serif;\n  font-size: 10px;\n  padding-top: 20px; }\n@media (max-width: 575.98px) {\n  .user {\n    margin-top: 150px; } }\nfieldset {\n  border: none;\n  padding-top: 25px; }\ninput {\n  border: none;\n  background: transparent;\n  border-bottom: 1.3px solid #e8e8e8;\n  outline: none;\n  padding: 10px;\n  font-size: 14px;\n  width: 200px;\n  border-radius: 4px;\n  margin: 4px 2px 3px 2px; }\nbutton {\n  margin-top: 10px;\n  width: 220px;\n  height: 33px;\n  border: none;\n  border-radius: 3px;\n  font-weight: bold;\n  font-size: 17px;\n  outline: none;\n  cursor: pointer;\n  color: #ffffff;\n  background: #4167b2; }\n.sign-up {\n  color: #ffffff;\n  font-size: 17px; }\nh1 {\n  color: #585e57;\n  letter-spacing: 1px;\n  padding-top: 5px; }\np {\n  color: #6f6a6a;\n  margin: 14px 2px 3px 2px; }\na {\n  color: #09bf9a;\n  text-decoration: none; }\n#warning {\n  color: #4167b2;\n  font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/user-account/user-account.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-account/user-account.component.ts ***!
  \********************************************************/
/*! exports provided: UserAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAccountComponent", function() { return UserAccountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/e-com-store-api.service */ "./src/app/services/e-com-store-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserAccountComponent = /** @class */ (function () {
    /**Logic to identify as a user using the services */
    function UserAccountComponent(service, router, userService) {
        this.service = service;
        this.router = router;
        this.userService = userService;
    }
    UserAccountComponent.prototype.ngOnInit = function () {
    };
    UserAccountComponent.prototype.check = function (event) {
        console.log(event);
        this.email = event.target.value;
    };
    UserAccountComponent.prototype.check2 = function (event) {
        this.password = event.target.value;
        console.log(event);
    };
    UserAccountComponent.prototype.submit = function () {
        var _this = this;
        console.log("email:", this.email, "password:", this.password);
        this.service.login({ password: this.password, email: this.email }).subscribe(function (value) {
            console.log(value);
            _this.service.setToken(value['token']);
            console.log(value);
            _this.service.getById(value['userId']).subscribe(function (value) {
                console.log(value);
                _this.router.navigate(['']);
                _this.service.setLogged(true);
                _this.userService.setUser(value['user']);
            });
        });
    };
    UserAccountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-account',
            template: __webpack_require__(/*! ./user-account.component.html */ "./src/app/user-account/user-account.component.html"),
            styles: [__webpack_require__(/*! ./user-account.component.scss */ "./src/app/user-account/user-account.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_3__["EComStoreApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], UserAccountComponent);
    return UserAccountComponent;
}());



/***/ }),

/***/ "./src/app/user-log-out/user-log-out.component.html":
/*!**********************************************************!*\
  !*** ./src/app/user-log-out/user-log-out.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  user-log-out works!\n</p>\n"

/***/ }),

/***/ "./src/app/user-log-out/user-log-out.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/user-log-out/user-log-out.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user-log-out/user-log-out.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-log-out/user-log-out.component.ts ***!
  \********************************************************/
/*! exports provided: UserLogOutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLogOutComponent", function() { return UserLogOutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/e-com-store-api.service */ "./src/app/services/e-com-store-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserLogOutComponent = /** @class */ (function () {
    function UserLogOutComponent(service, router, userService) {
        this.service = service;
        this.router = router;
        this.userService = userService;
    }
    UserLogOutComponent.prototype.ngOnInit = function () {
        this.userService.setUser({});
    };
    UserLogOutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-log-out',
            template: __webpack_require__(/*! ./user-log-out.component.html */ "./src/app/user-log-out/user-log-out.component.html"),
            styles: [__webpack_require__(/*! ./user-log-out.component.scss */ "./src/app/user-log-out/user-log-out.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_3__["EComStoreApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], UserLogOutComponent);
    return UserLogOutComponent;
}());



/***/ }),

/***/ "./src/app/user-sign-up/user-sign-up.component.html":
/*!**********************************************************!*\
  !*** ./src/app/user-sign-up/user-sign-up.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row  mr-2 d-block-lg mt-5\">\n    <div class=\"col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2\">     \n      <form class=\"form-horizontal\" method=\"post\" action=\"#\">\n        <br>\n        <fieldset class=\"sign-up-resigtre\">\n          <legend id=\"inscript\">Créez un compte</legend>\t\t\t\n          <div class=\"form-group\">\n            <label for=\"name\" class=\"cols-sm-2 control-label\">Nom</label>\n            <div class=\"cols-sm-10\">\n              <div class=\"input-group\">\n                <span class=\"input-group-addon\"><i class=\"fa fa-user fa\" aria-hidden=\"true\"></i></span>\n                <input type=\"text\" class=\"form-control\" name=\"name\" id=\"name\"  placeholder=\"Entrez votre Nom\" required (change)=\"checkName($event);\"mkn/>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label for=\"username\" class=\"cols-sm-2 control-label\">Nom utilisateur</label>\n            <div class=\"cols-sm-10\">\n              <div class=\"input-group\">\n                <span class=\"input-group-addon\"><i class=\"fa fa-users fa\" aria-hidden=\"true\"></i></span>\n                <input type=\"text\" class=\"form-control\" name=\"username\" id=\"username\"  placeholder=\"Entrez votre nom utilisateur\"  (change)=\"checkUserName($event);\"mkn/>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label for=\"email\" class=\"cols-sm-2 control-label\">Email</label>\n            <div class=\"cols-sm-10\">\n              <div class=\"input-group\">\n                <span class=\"input-group-addon\"><i class=\"fa fa-envelope fa\" aria-hidden=\"true\"></i></span>\n                <input type=\"text\" class=\"form-control\" name=\"email\" id=\"email\"  placeholder=\"Entrez votre Email\"  (change)=\"checkEmail($event);\"mkn/>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label for=\"password\" class=\"cols-sm-2 control-label\">Mot de passe</label>\n            <div class=\"cols-sm-10\">\n              <div class=\"input-group\">\n                <span class=\"input-group-addon\"><i class=\"fa fa-lock fa-lg\" aria-hidden=\"true\"></i></span>\n                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\"  placeholder=\"Entrez votre mot de passe\" (change)=\"checkPassWord($event);\"mkn/>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label for=\"confirm\" class=\"cols-sm-2 control-label\">Confirmez mot de passe</label>\n            <div class=\"cols-sm-10\">\n              <div class=\"input-group\">\n                <span class=\"input-group-addon\"><i class=\"fa fa-lock fa-lg\" aria-hidden=\"true\"></i></span>\n                <input type=\"password\" class=\"form-control\" name=\"confirm\" id=\"confirm\"  placeholder=\"Confirmez mot de passe\" (change)=\"checkConfPassword($event);\"mkn/>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group \">\n            <button (click)=\"submit()\" type=\"button\" class=\"btn btn-resgistre btn-lg btn-block login-button\">Enregistrer</button>\n          </div>\n        </fieldset>\n      </form>\n    </div>\n  </div>\n</div>  "

/***/ }),

/***/ "./src/app/user-sign-up/user-sign-up.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/user-sign-up/user-sign-up.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-resgistre {\n  background: #4167b2;\n  font-weight: bold;\n  font-size: 17px;\n  outline: none;\n  cursor: pointer;\n  color: #ffffff; }\n\n.sign-up-resigtre {\n  margin-top: 100px; }\n\n#inscript {\n  color: #4267b2;\n  font-size: 18px;\n  font-family: 'hdoicons' !important;\n  font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/user-sign-up/user-sign-up.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-sign-up/user-sign-up.component.ts ***!
  \********************************************************/
/*! exports provided: UserSignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSignUpComponent", function() { return UserSignUpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/e-com-store-api.service */ "./src/app/services/e-com-store-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserSignUpComponent = /** @class */ (function () {
    function UserSignUpComponent(service, router) {
        this.service = service;
        this.router = router;
    }
    UserSignUpComponent.prototype.ngOnInit = function () {
    };
    UserSignUpComponent.prototype.checkName = function (event) {
        console.log(event);
        this.name = event.target.value;
    };
    UserSignUpComponent.prototype.checkEmail = function (event) {
        console.log(event);
        this.email = event.target.value;
    };
    UserSignUpComponent.prototype.checkPassWord = function (event) {
        console.log(event);
        this.password = event.target.value;
    };
    UserSignUpComponent.prototype.checkConfPassword = function (event) {
        console.log(event);
        this.confirmPassword = event.target.value;
    };
    UserSignUpComponent.prototype.checkUserName = function (event) {
        console.log(event);
        this.userName = event.target.value;
    };
    UserSignUpComponent.prototype.submit = function () {
        var _this = this;
        var newUser = { name: this.name, password: this.confirmPassword, email: this.email, userName: this.userName };
        console.log("new user", newUser);
        this.service.newUser(newUser).subscribe(function (value) {
            console.log(value);
            _this.router.navigate(["user", value]);
            //this.service.user = value;
            console.log(value);
        });
    };
    UserSignUpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-sign-up',
            template: __webpack_require__(/*! ./user-sign-up.component.html */ "./src/app/user-sign-up/user-sign-up.component.html"),
            styles: [__webpack_require__(/*! ./user-sign-up.component.scss */ "./src/app/user-sign-up/user-sign-up.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_com_store_api_service__WEBPACK_IMPORTED_MODULE_2__["EComStoreApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], UserSignUpComponent);
    return UserSignUpComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/diallo/frontend-certif-promo4/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map